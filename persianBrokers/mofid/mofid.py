import asyncio
import json
import logging
import time

from persianBrokers.base_broker import BaseBroker, CreatedOrderStatus
from persianBrokers.exception import TokenException
from persianBrokers.selenuim_storage import LocalStorage

logger = logging.getLogger()


class Mofid(BaseBroker):
    async def buy(self, isin, order_count, order_price, timeout=None):
        url = "https://easy-api.emofid.com/easy/api/OmsOrder"

        payload = {
            "isin": isin,
            "financeId": 1,
            "quantity": order_count,
            "price": order_price,
            "side": 0,
            "validityType": 74,
            "easySource": 1,
            "cautionAgreementSelected": False
        }
        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json',
        }

        return await self.request(side='buy', url=url, payload=payload, headers=headers, timeout=timeout)

    async def sell(self, isin, order_count, order_price, timeout=None):
        url = "https://easy-api.emofid.com/easy/api/OmsOrder"
        payload = {
            "isin": isin,
            "financeId": 1,
            "quantity": int(order_count),
            "price": int(order_price),
            "side": 1,
            "validityType": 74,
            "easySource": 1,
            "cautionAgreementSelected": False
        }
        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json',
        }
        return await self.request(side='sell', url=url, payload=payload, headers=headers, timeout=timeout)


    async def parse_response(self, response: dict) -> CreatedOrderStatus:
        is_successful = response.get('isSuccessfull', False)
        # TODO: check broker_order_id when broker is open
        broker_order_id = response.get("omsOrderId")
        broker_msg = response.get("message")
        order_status = CreatedOrderStatus(is_successful=is_successful,
                                          broker_msg=broker_msg,
                                          broker_order_id=broker_order_id)
        return order_status

    async def wait(self):
        await asyncio.sleep(0.300)

    @classmethod
    def get_token(cls, username, password):
        """
        Return token using password and username with selenium.
        """
        from selenium import webdriver
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')

        easy_trader_page = webdriver.Chrome(options=chrome_options, service_log_path=None)
        local_storage = LocalStorage(driver=easy_trader_page)
        try:
            for i in range(3):
                logger.info(f"Trying for {i} time to get token from Mofid.")
                easy_trader_page.get("https://account.emofid.com/Login")
                username_item = easy_trader_page.find_element_by_id("Username")

                username_item.clear()
                username_item.send_keys(username)

                password_item = easy_trader_page.find_element_by_id("Password")
                password_item.clear()
                password_item.send_keys(password)

                login_btn = easy_trader_page.find_element_by_id("submit_btn")
                login_btn.click()
                time.sleep(5)
                easy_trader_page.get("https://d.easytrader.emofid.com/")
                time.sleep(10)

                # Try to find access_token in local storage keys and their values if are dictionary
                all_storage = local_storage.items()
                access_token = None
                for key, value in all_storage.items():
                    if key == "access_token":
                        access_token = value
                        break
                    else:
                        try:
                            j_value = json.loads(value)
                            if isinstance(j_value, dict) and "access_token" in j_value:
                                access_token = j_value["access_token"]
                                break
                        except json.JSONDecodeError:
                            continue
                else:
                    logger.warning(f"Can't find token from Mofid :) {local_storage.items()} is the storage")

                if access_token:
                    break
            else:
                raise TokenException("Token in Mofid broker has not be found.")
            return access_token, None

        except Exception:
            logger.error("Error during get token from mofid", exc_info=True)

        finally:
            easy_trader_page.close()


async def main():
    token = ''
    f = Mofid(token=token)
    r = await f.sell(isin='IRO1SORB0001',
                     order_price=3,
                     order_count=3)

if __name__ == "__main__":
    # token, _ = Mofid.get_token(username="", password="")
    # print(f"Here is the token {token}")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
