from importlib import import_module

from persianBrokers.base_broker import BaseBroker


def get_broker(name: str) -> BaseBroker:
    name = name.lower()
    broker_module = import_module(f"persianBrokers.{name}.{name}")
    # Convert agah to Agah, or mobinsarmaye to Mobinsarmaye
    broker_class_name = name.capitalize()
    broker_class = getattr(broker_module, broker_class_name)
    return broker_class
