import os
import easyocr


def get_captcha(image_path: str, captcha_length: int = 4):
    file_name = os.path.basename(image_path)
    captcha_dir = os.path.dirname(image_path)

    reader = easyocr.Reader(['en'], gpu=False)
    result = reader.readtext(image_path)
    if not (result and result[0] and result[0][1]):
        renamed_path = os.path.join(captcha_dir, f"unrecognizedCaptcha_{file_name}")
        os.rename(image_path, renamed_path)
        return None, renamed_path
    found_number = result[0][1]
    name = [item for item in found_number if item.isdigit()]
    if name and len(name) == captcha_length:
        renamed_path = os.path.join(captcha_dir, f'recognized_{"".join(name)}_{file_name}')
        os.rename(image_path, renamed_path)
        return int("".join(name)), renamed_path
    if name:
        renamed_path = os.path.join(captcha_dir, f'wrongCountRecognized_{"".join(name)}_{file_name}')
    else:
        renamed_path = os.path.join(captcha_dir, f"unrecognizedCaptcha_{file_name}")
    os.rename(image_path, renamed_path)
    return None, renamed_path
