import asyncio
import logging
import os.path
import random
import string
import time
import urllib.request
from persianBrokers.base_broker import BaseBroker, CreatedOrderStatus
from persianBrokers.exception import TokenException

logger = logging.getLogger()


class Mobinsarmaye(BaseBroker):
    async def buy(self, isin: str, order_count: int, order_price: int, timeout=None):
        url = "https://api3.mobinsb.ir/web/v1/Order/Post"

        payload = {
            "orderCount": order_count,
            "orderPrice": order_price,
            "FinancialProviderId": 1,
            "isin": isin,
            "orderSide": 65,
            "orderValidity": 74,
            "orderValiditydate": "",
            "maxShow": 0,
            "orderId": 0
        }
        headers = {
            'Authorization': F'BasicAuthentication {self.token}',
            'Content-Type': 'application/json',

            "Host": "api3.mobinsb.ir",
            "Connection": "keep-alive",
            "sec-ch-ua": '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            "DNT": "1",
            "Accept-Language": "fa",
            "sec-ch-ua-mobile": "?0",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36",
            "X-Requested-With": "XMLHttpRequest",
            "sec-ch-ua-platform": "Windows",
            "Accept": "*/*",
            "Origin": "https://mobile.mobinsb.ir",
            "Sec-Fetch-Site": "same-site",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Dest": "empty",
            "Referer": "https://mobile.mobinsb.ir/",
            "Accept-Encoding": "gzip, deflate, br",
        }
        return await self.request(side='buy', url=url, payload=payload, headers=headers, timeout=timeout)

    async def sell(self, isin, order_count, order_price, timeout=None):
        url = "https://api3.mobinsb.ir/web/v1/Order/Post"

        payload = {
            "orderCount": int(order_count),
            "orderPrice": int(order_price),
            "FinancialProviderId": 1,
            "isin": isin,
            "orderSide": 86,
            "orderValidity": 74,
            "orderValiditydate": "",
            "maxShow": 0,
            "orderId": 0
        }
        headers = {
            'Authorization': F'BasicAuthentication {self.token}',
            'Content-Type': 'application/json',

            "Host": "api3.mobinsb.ir",
            "Connection": "keep-alive",
            "sec-ch-ua": '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
            "DNT": "1",
            "Accept-Language": "fa",
            "sec-ch-ua-mobile": "?0",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36",
            "X-Requested-With": "XMLHttpRequest",
            "sec-ch-ua-platform": "Windows",
            "Accept": "*/*",
            "Origin": "https://mobile.mobinsb.ir",
            "Sec-Fetch-Site": "same-site",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Dest": "empty",
            "Referer": "https://mobile.mobinsb.ir/",
            "Accept-Encoding": "gzip, deflate, br",
        }
        return await self.request(side='sell', url=url, payload=payload, headers=headers, timeout=timeout)

    async def parse_response(self, response: dict) -> CreatedOrderStatus:
        is_successful = response.get('IsSuccessfull', False)
        broker_msg = response.get("MessageDesc") or response.get("MessageCode")
        broker_order_id = response.get("Data", {}).get("orderId")
        order_status = CreatedOrderStatus(is_successful=is_successful,
                                          broker_msg=broker_msg,
                                          broker_order_id=broker_order_id)
        return order_status

    async def wait(self):
        await asyncio.sleep(0.3)

    @classmethod
    def get_token(cls, username, password):
        """
        Returns access_token and refresh token.
        Mote: Mobinsarmaye has not refresh_token so always return none for access_token (second return value)
        Args:
            username:
            password:

        Returns: tuple(str, str)

        """
        from persianBrokers.ocr import get_captcha
        from selenium import webdriver
        if username is None or password is None:
            raise ValueError("username and password can not be null in get token.")
        token = None

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')\

        mobinsarmaye_page = webdriver.Chrome(options=chrome_options, service_log_path=None)
        for try_count in range(5):
            try:
                logger.info(f"Try get token from mobinsarmaye for {try_count} try number.")
                mobinsarmaye_page.get("https://silver.mobinsb.ir/Account/Login")
                time.sleep(5)
                username_item = mobinsarmaye_page.find_element_by_id("txtusername")
                username_item.clear()
                username_item.send_keys(username)

                password_item = mobinsarmaye_page.find_element_by_id("txtpassword")
                password_item.clear()
                password_item.send_keys(password)

                captcha_item = mobinsarmaye_page.find_element_by_id("captcha-img-plus")
                src = captcha_item.get_attribute('src')

                # download the image
                captcha_name = f'{"".join(random.choices(string.ascii_letters, k=10))}.jpeg'
                current_file_path = os.path.dirname(os.path.abspath(__name__))
                abs_captcha_root = os.path.join(current_file_path, f"captcha")
                os.makedirs(abs_captcha_root, exist_ok=True)

                abs_captcha_path = os.path.join(abs_captcha_root, captcha_name)

                res = urllib.request.urlretrieve(src, abs_captcha_path)
                #  Set cookies related to captcha
                cookies = [item[1].split(";")[0].split("=") for item in res[1]._headers if "Set-Cookie" in item[0]]
                cookies_dict = [{"name": cookie[0], "value": cookie[1]} for cookie in cookies]
                [mobinsarmaye_page.add_cookie(cok) for cok in cookies_dict]
                captcha, captcha_new_path = get_captcha(abs_captcha_path)
                if not captcha:
                    continue
                captcha_item_input = mobinsarmaye_page.find_element_by_name("capcha")
                captcha_item_input.send_keys(captcha)
                mobinsarmaye_page.find_element_by_css_selector(
                    "form[name='loginForm']>div.row.button-holder>div.col-md-12>button"
                ).click()
                time.sleep(2)
                url = mobinsarmaye_page.current_url
                if "/Home/Default/" not in url:
                    continue
                time.sleep(1)
                token = mobinsarmaye_page.get_cookie("__apitoken__")["value"]

                # This captcha was guess correct so delete it (juts wrong captcha will be store in storage to bug fix)
                os.remove(captcha_new_path)
                break
            except Exception:
                token = None
                logger.error("Error during get token of mobinsarmaye", exc_info=True)
                continue

        mobinsarmaye_page.quit()
        if token is None:
            raise TokenException("Can't get token from mobinsarmaye.")
        return token, None


async def main():
    f = Mobinsarmaye(token='')
    r = await f.sell(isin='IRO1SORB0001',
                     order_price=3,
                     order_count=3)

if __name__ == "__main__":
    # token, _ = Mobinsarmaye.get_token(username="", password="")
    # print(f"Here is the token {token}")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())


