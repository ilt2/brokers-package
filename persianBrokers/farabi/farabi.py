import asyncio
import json
import logging
import os
import random
import string
import time
import base64

from persianBrokers.base_broker import BaseBroker, CreatedOrderStatus
from persianBrokers.exception import TokenException
from persianBrokers.selenuim_storage import LocalStorage

logger = logging.getLogger()


class Farabi(BaseBroker):
    async def buy(self, isin, order_count, order_price, timeout=None):
        url = "https://tse.farabixo.com/api/Order/add-order"

        payload = {
            "instrumentIdentification": isin.upper(),
            "price": order_price,
            "quantity": order_count,
            "orderExecutionType": "app-web",
            "investorBourseCodeId": 0,
            "orderEntryDeviceType": 3,
            "accountRouteType": 1,
            "orderSide": 1,
            "validityType": 1,
            "parentId": 0
        }
        headers = {
            'Authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json',
            'Host': 'tse.farabixo.com',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'Accept': 'application/json',
            'User-Agent': 'Mozilla/5.0 (X11; Windows x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
            'sec-ch-ua-platform': 'Windows',
            'Origin': 'https://app.farabixo.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://app.farabixo.com/',
            'Accept-Language': 'fa'
        }

        return await self.request(side='buy', url=url, payload=payload, headers=headers, timeout=timeout)

    async def sell(self, isin, order_count, order_price, timeout=None):
        url = "https://tse.farabixo.com/api/Order/add-order"
        payload = {
            "instrumentIdentification": isin,
            "price": order_price,
            "quantity": order_count,
            "orderExecutionType": "app-web",
            "investorBourseCodeId": 0,
            "orderEntryDeviceType": 3,
            "accountRouteType": 1,
            "orderSide": 2,
            "validityType": 1,
            "parentId": 0
        }

        headers = {
            'authorization': 'Bearer ' + self.token,
            'Content-Type': 'application/json',
            'Host': 'tse.farabixo.com',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
            'Accept': 'application/json',
            'User-Agent': 'Mozilla/5.0 (X11; Windows x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
            'sec-ch-ua-platform': 'Windows',
            'Origin': 'https://app.farabixo.com',
            'Sec-Fetch-Site': 'same-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://app.farabixo.com/',
            'Accept-Language': 'fa'
        }
        return await self.request(side='sell', url=url, payload=payload, headers=headers, timeout=timeout)

    async def parse_response(self, response: dict) -> CreatedOrderStatus:
        is_successful = response.get('success', False)
        broker_order_id = response.get("data", {}).get("orderId")
        broker_msg = " AND ".join(response.get("msg", "-")) if isinstance(response.get("msg"), list) else response.get("msg")
        order_status = CreatedOrderStatus(is_successful=is_successful,
                                          broker_msg=broker_msg,
                                          broker_order_id=broker_order_id)
        return order_status

    async def wait(self):
        await asyncio.sleep(0.300)

    def refresh_token(self):
        """
        Refresh token using refresh_token
        """
        url = None

    @classmethod
    def get_token(cls, username, password):
        from selenium import webdriver
        from persianBrokers.ocr import get_captcha

        if username is None or password is None:
            raise ValueError("username and password can not be null in get token.")
        token = None
        refresh_token = None

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')

        farabi_page = webdriver.Chrome(options=chrome_options, service_log_path=None)
        local_storage = LocalStorage(driver=farabi_page)
        for try_count in range(10):
            try:
                logger.info(f"Try get token from farabi for {try_count} try number.")
                farabi_page.get("https://app.farabixo.com/AuthNavigator/Login")
                time.sleep(3)
                username_item = farabi_page.find_element_by_xpath('//input[@autocomplete="username"]')
                username_item.clear()
                username_item.send_keys(username)

                password_item = farabi_page.find_element_by_xpath("//input[@autocomplete='password']")
                password_item.clear()
                password_item.send_keys(password)

                # download the image
                captcha_item = farabi_page.find_element_by_xpath("//img[@draggable='false']")
                base_64_captcha: str = captcha_item.get_attribute('src')
                base_64_captcha = base_64_captcha.split(",")[1]
                if len(base_64_captcha) > 1:
                    base_64_captcha = ",".join(base_64_captcha)

                captcha_name = f'{"".join(random.choices(string.ascii_letters, k=10))}.jpeg'
                current_file_path = os.path.dirname(os.path.abspath(__name__))
                abs_captcha_root = os.path.join(current_file_path, f"captcha")
                os.makedirs(abs_captcha_root, exist_ok=True)

                abs_captcha_path = os.path.join(abs_captcha_root, captcha_name)

                with open(abs_captcha_path, "wb") as fh:
                    decoded_captcha_img = base64.b64decode(base_64_captcha)
                    fh.write(decoded_captcha_img)

                captcha, captcha_new_path = get_captcha(abs_captcha_path)
                if not captcha:
                    continue
                captcha_item_input = farabi_page.find_element_by_xpath("//input[@placeholder='کد امنیتی']")
                captcha_item_input.send_keys(captcha)
                time.sleep(2)
                farabi_page.find_element_by_xpath("//div[text()='ورود']").click()
                time.sleep(5)
                url = farabi_page.current_url
                if "home/MarketMap" not in url:
                    continue
                time.sleep(1)
                try:
                    token_dict = json.loads(json.loads(LocalStorage(farabi_page).items()['persist:user'])["token"])
                    token = token_dict["accessToken"]
                    refresh_token = token_dict["refreshToken"]
                except Exception:
                    raise TokenException(f"Expected token and refresh token be in local storage of farabixo but not "
                                         f"exists! {local_storage.items()}")

                # This captcha was guess correct so delete it (juts wrong captcha will be store in storage to bug fix)
                os.remove(captcha_new_path)
                break
            except Exception:
                token = None
                logger.error("Error during get token of farabi", exc_info=True)
                continue

        farabi_page.quit()
        if token is None:
            raise TokenException("Can't get token from farabi.")
        return token, refresh_token


async def main():
    f = Farabi(token='')
    r = await f.sell(isin='IRO1SORB0001',
               order_price=3,
               order_count=3)
    print(f"The order inserted {'successfully.' if r else 'failed.'}")


if __name__ == "__main__":
    # token, refresh_token = Farabi.get_token(username="", password="")
    # print("token is:", token)
    # print("refresh token is:", refresh_token)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
