import json
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Union

import aiohttp
import requests


@dataclass(frozen=True)
class CreatedOrderStatus:
    is_successful: bool  # is this try success
    broker_msg: str  # the message from broker response
    broker_order_id: Union[str, None] = None
    ilt_error: bool = False  # is error in ilt side?


class BaseBroker(ABC):
    logger = logging.getLogger()

    def __init__(self, token, refresh_token=None):
        self.token = token
        self.refresh_token = refresh_token

    async def request(self, side, url, payload, headers, timeout=None, method="POST") -> CreatedOrderStatus:
        response = None
        try:
            async with aiohttp.ClientSession() as async_session:
                async with async_session.request(method=method, url=url, json=payload, headers=headers,
                                                 timeout=timeout) as response:
                    if response.status // 100 == 2:  # if status is in 2XX means request was ok!
                        response = await response.json()
                        self.logger.debug(
                            f"{side} request sent successfully, {json.dumps(payload)}, {self.__class__.__name__}: "
                            f"{json.dumps(response)}")
                        return await self.parse_response(response)
                    else:
                        msg = f"{side} request failed, {json.dumps(payload)}, {self.__class__.__name__}: " \
                              f"status_code:{response.status}, response_content={await response.text()}"
                        self.logger.warning(msg)
                        return CreatedOrderStatus(is_successful=False, broker_msg=msg)

        except Exception as e:
            msg = f"error during {side} {json.dumps(payload)},{self.__class__.__name__}," \
                  f" response={response.text if response is not None else None}" \
                  f" status_code={response.status if response is not None else None} ans reason = {str(e)}"
            self.logger.error(msg, exc_info=True)
            return CreatedOrderStatus(is_successful=False, broker_msg=msg, ilt_error=True)

    @abstractmethod
    async def parse_response(self, response):
        raise NotImplemented()

    @abstractmethod
    async def wait(self):
        raise NotImplemented()

    @abstractmethod
    async def buy(self, isin, order_count, order_price, timeout=None):
        raise NotImplemented()

    @abstractmethod
    async def sell(self, isin, order_count, order_price, timeout=None):
        raise NotImplemented()

    @abstractmethod
    async def get_token(self, username, password) -> (str, str):
        """
        Returns access_token and refresh_token
        Args:
            username:
            password:

        Returns:

        """
        raise NotImplemented