import os

from setuptools import setup, find_packages

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
os.chdir(ROOT_DIR)

with open("README.md", "r") as fh:
    long_description = fh.read()


setup(name='persianBrokers',
      version='0.7.4',
      description='Fix logging response during success response',
      author='M.Mortaz,M.Moalaghi',
      author_email='hdhshd@dd.com',
      long_description=long_description,
      long_description_content_type="text/markdown",
      packages=find_packages(),
      install_requires=[
          'aiohttp==3.8.1',
          'aiosignal==1.2.0',
          'async-timeout==4.0.2',
          'attrs==21.4.0',
          'certifi==2021.5.30',
          'charset-normalizer==2.0.6',
          'cycler==0.10.0',
          'frozenlist==1.2.0',
          'idna==3.2',
          'imageio==2.9.0',
          'kiwisolver==1.3.2',
          'matplotlib==3.4.3',
          'multidict==5.2.0',
          'networkx==2.6.3',
          'numpy==1.21.2',
          'Pillow==8.2.0',
          'pyparsing==2.4.7',
          'python-bidi==0.4.2',
          'python-dateutil==2.8.2',
          'PyWavelets==1.1.1',
          'PyYAML==5.4.1',
          'requests==2.26.0',
          'scikit-image==0.18.3',
          'scipy==1.7.1',
          'six==1.16.0',
          'tifffile==2021.8.30',
          'typing-extensions==3.10.0.2',
          'urllib3==1.26.6',
          'yarl==1.7.2',
      ],
      extras_require={
          "token_ability": [
              'selenium==3.141.0',
              'easyocr==1.4.1',
              'torch==1.9.0',
              'torchvision==0.10.0',
              'opencv-python-headless==4.5.3.56',

          ]
      },
      include_package_data=True
      )
