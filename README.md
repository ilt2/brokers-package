# Persian Brokers API package
This package is Persian brokers client, which has the following capabilities:
1. Send orders.
2. Login automatically with username and password (also for captcha login)

## dependency
###  selenium chrome webdriver if you need get brokers token from username and password
To use auto login, and get the brokers token needs to install google-chrome, and it's driver:
https://chromedriver.chromium.org/downloads  # ChromeDriver 101.0.4951.41
download google-chrome v 101.0.4951.41
## Installation
### To use for auto login feature
If you want to use package to get token from brokers, you need use `selenium` and `easyocr`. to use selenium needs to install chrome, and it's driver
both webdriver and chrome added to webdriver_asset folder. to install them do: 
1. add webdriver path to OS PATH:
```shell
sudo cp webdrivers_assets/chromedriver /usr/bin/local
```

2. Install google-chrome
```
sudo apt update 
sudo apt install -f ./webdrivers_assets/google-chrome-101.0.4951.41_amd64.deb -y
```
3. Install dependencies:
```shell
pip install -e ".[token_ability]"
```
or
```shell
pip install dist/persianBrokers-X.X.X.tar.gz"[token_ability]"
```
### Just for API
If you wish to use API of broker without auto login (needs pass token manually) the not need to install easyocr package (with more than 1G volume)

```shell
pip install -e .
``` 
or
```shell
pip install dist/persianBrokers-X.X.X.tar.gz
```

### easyocr
To login platforms with captcha needs, to solve guess the captcha we are using easyocr, which will be automatically installed and used:
https://github.com/JaidedAI/EasyOCR
